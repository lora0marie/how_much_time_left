function time_left(){
    // get input time
    var end = document.getElementById("end").value.split(":");
    let end_h = end[0];
    if(end_h == '00') {end_h = 24}
    let end_m = end[1];
    // current time
    var now = new Date;
    let now_h = now.getHours();
    let now_m = now.getMinutes();

    let left_h = end_h - now_h
    let left_m = end_m - now_m;
    if(left_m < 0){
        left_m = 60 + left_m;
        left_h = left_h - 1;
    }
    if(left_h < 0){
        left_h = 24 + left_h;
    }
    let left_s = 60 - now.getSeconds()

    document.getElementById("time").innerHTML = left_h + ' : ' + left_m + ' : ' + left_s;
    setTimeout('time_left();','1000');
    return true;
}

function set_hour(h,i,s){
    var hour = new Date;
    hour.setHours(h,i,s);
    document.getElementById("end").value = hour.toString().substring(16,21);
    time_left();
}
